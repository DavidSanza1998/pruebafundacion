var pathname = $("meta[name=pathname]").attr("content");
$( document ).ready(function() {

  $("#guardar").click(function(){
    var idDato = $("#idDato").val();
    var idTipoDocumento =  $('#idTipoDocumento').val();
    var ideDatos =  $('#ideDatos').val();
    var nomDatos =  $('#nomDatos').val();
    var apeDatos =  $('#apeDatos').val();
    var telDatos =  $('#telDatos').val();
    var fecDatos =  $('#fecDatos').val();
    var horDatos =  $('#horDatos').val();

     if(idTipoDocumento== null || ideDatos=="" || nomDatos=="" || apeDatos=="" || telDatos=="" || fecDatos=="" || horDatos==""){
       toastr.error("Faltan Campos por llenar, Todos los datos son obligatorios.", "Error");
     }else{
       var parametros = {
         "idDato":idDato,
         "idTipoDocumento":idTipoDocumento,
         "ideDatos":ideDatos,
         "nomDatos":nomDatos,
         "apeDatos":apeDatos,
         "telDatos":telDatos,
         "fecDatos":fecDatos,
         "horDatos":horDatos,
       }
      $.ajax({
        type : 'post',
        url: pathname+'/ajax_save_datos',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:parametros,
        success: function(data){
          $("#idDato").val(data.id);
          toastr.success("Guardo Correctamente el Dato en la base de datos");
          $("#tabla").empty();
          $("#tabla").append("<h1 class='col-md-offset-5'>Datos Guardados</h1>"+
                              "<div class='row'>"+
                                "<table id='tabla' class='table table-striped table-hover'>"+
                                  "<thead>"+
                                    "<tr>"+
                                      "<th>Nombre</th>"+
                                      "<th>Apellido</th>"+
                                      "<th>Telefono</th>"+
                                      "<th>Numero Identificación</th>"+
                                      "<th>Fecha y Hora</th>"+
                                    "</tr>"+
                                  "</thead>"+
                                  "<tbody>"+
                                    "<tr>"+
                                      "<td>"+data.nomDatos+"</td>"+
                                      "<td>"+data.apeDatos+"</td>"+
                                      "<td>"+data.telDatos+"</td>"+
                                      "<td>"+data.ideDatos+"</td>"+
                                      "<td>"+data.fecDatos+"</td>"+
                                    "</tr>"+
                                  "</tbody>"+
                                "</table>"+
                              "</div");

        },
         fail: function(response){
           toastr.error("Ocurrio un Error, intente nuevamente");
         }
      });
     }
  });
});

$('#horDatos').mask('00:00:00');
