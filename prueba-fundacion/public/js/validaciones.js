  //Validacion solo letras
  function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
    especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial){
        return false;
      }
}
  //validacion cada primera letra
  function cadaprimera(solicitar){
    var index;
    var tmpStr;
    var tmpChar;
    var preString;
    var postString;
    var strlen;
    tmpStr = solicitar.value.toLowerCase();
    strLen = tmpStr.length;
    if (strLen > 0)
    {
        for (index = 0; index < strLen; index++)
        {
            if (index == 0)
            {
                tmpChar = tmpStr.substring(0,1).toUpperCase();
                postString = tmpStr.substring(1,strLen);
                tmpStr = tmpChar + postString;
            }
            else
            {
                tmpChar = tmpStr.substring(index, index+1);
            if (tmpChar == " " && index < (strLen-1))
            {
                tmpChar = tmpStr.substring(index+1, index+2).toUpperCase();
                preString = tmpStr.substring(0, index+1);
                postString = tmpStr.substring(index+2,strLen);
                tmpStr = preString + tmpChar + postString;
            }
            }
        }
    }
    solicitar.value = tmpStr;
}

// validacion SOLO NUMEROS
 function SoloNumerosEnteros(evt){
   if(window.event){//asignamos el valor de la tecla a keynum
      keynum = evt.keyCode; //IE
   }
   else{
      keynum = evt.which; //FF
   }
  //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
   if((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6 ){
      return true;
   }
   else{
      return false;
   }
}
