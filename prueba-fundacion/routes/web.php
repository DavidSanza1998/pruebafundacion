<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {

  Route::get('/datos', 'DatoController@index')->name('datos.index');

  Route::get('/datos/create', 'DatoController@create')->name('datos.create');

  Route::post('/datos/store', 'DatoController@store')->name('datos.store');

  Route::get('datos/{dato}/edit', 'DatoController@edit')->name('datos.edit');

  Route::delete('datos/{dato}', 'DatoController@destroy')->name('datos.destroy');

  //rutas ajax
  Route::post('/ajax_save_datos', 'DatoController@ajax_save_datos')->name('datos.ajax_save_datos');

});
