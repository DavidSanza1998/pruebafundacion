@extends('layouts.app')
  @section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <a href="{{route('datos.index')}}" class="btn btn-warning"><i class="icon-reply icon-large" aria-hidden="true"></i> Regresar</a>
        </div>
      </div>
      <div class="container" id="tabla">

      </div>
      <div class="row">
        <div class="col-md-offset-5">
          <h1>Actualizar Datos</h1>
        </div>
      </div>
      <form>
        <input type="hidden" name="idDato" value="{{$datos->id}}" id="idDato">
        <div class="form-group">
          <select class="form-control" name="idTipoDocumento" id="idTipoDocumento">
            <option selected disabled>Seleccione el Tipo de Documento</option>
            @foreach($tipodocumentos as $tipodocumento)
              @if($tipodocumento->id == $datos->idTipoDocumento)
                <option selected value="{{$tipodocumento->id}}">{{$tipodocumento->nomTipoDocumento}}</option>
              @else
                <option value="{{$tipodocumento->id}}">{{$tipodocumento->nomTipoDocumento}}</option>
              @endif
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Identificación</label>
          <input type="text" class="form-control" value="{{$datos->ideDatos}}" name="ideDatos" id="ideDatos" placeholder="Identificación">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" class="form-control" value="{{$datos->nomDatos}}" name="nomDatos" id="nomDatos" placeholder="Nombre">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Apellido</label>
          <input type="text" class="form-control" value="{{$datos->apeDatos}}" name="apeDatos" id="apeDatos" placeholder="Apellido">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Telefono</label>
          <input type="text" class="form-control" value="{{$datos->telDatos}}" name="telDatos" id="telDatos" placeholder="Telefono">
        </div>
        <?php
          $fecha_hora  = $datos->fecDatos;
          $resultado = explode(" ", $fecha_hora);
         ?>
        <div class="form-group">
          <label for="exampleInputPassword1">Fecha de Nacimiento</label>
          <input type="date" class="form-control" value="<?php echo $resultado[0]; ?>" name="fecDatos" id="fecDatos" placeholder="Fecha de Nacimiento">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Hora</label>
          <input type="text" name="horDatos" value="<?php echo $resultado[1]; ?>" class="form-control" id="horDatos" placeholder="Hora">
        </div>
        <button id="guardar" type="button" class="btn btn-success"><i class="icon-pencil icon-large"></i> Guardar</button>
      </form>
    </div>
    <script src="{{ asset('js/Ajax.js') }}"></script>
  @endsection
