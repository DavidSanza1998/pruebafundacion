@extends('layouts.app')
  @section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-offset-5">
          <h1>Lista de Personas</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-8" style="margin-bottom:1%;">
          <a href="{{route('datos.create')}}" class="btn btn-success pull-right"><i class="icon-plus-sign icon-large" aria-hidden="true"></i> Crear Persona</a>
        </div>
      </div>
      <div class="row">
        <table id="tabla" class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Telefono</th>
              <th>Numero Identificación</th>
              <th>Fecha y Hora</th>
              <th>Tipo Documento</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
            @foreach($datos as $dato)
              <tr>
                <td>{{$dato->nomDatos}}</td>
                <td>{{$dato->apeDatos}}</td>
                <td>{{$dato->telDatos}}</td>
                <td>{{$dato->ideDatos}}</td>
                <td>{{$dato->fecDatos}}</td>
                <td>{{$dato->nomTipoDocumento}}</td>
                <td>
                  <a href="{{ route('datos.edit', $dato->id) }}" class="btn btn-primary"><i class="icon-pencil icon-large"></i></a>
                </td>
                <td>
                  {!! Form::open(['route' => ['datos.destroy', $dato->id], 'method' => 'DELETE']) !!}
                    <button class="btn btn-danger"><i class="icon-trash icon-large"></i></button>
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <script>
      $(document).ready( function () {
        $('#tabla').DataTable();
      });
    </script>
  @endsection
