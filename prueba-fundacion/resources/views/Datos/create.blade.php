@extends('layouts.app')
  @section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <a href="{{route('datos.index')}}" class="btn btn-warning"><i class="icon-reply icon-large" aria-hidden="true"></i> Regresar</a>
        </div>
      </div>
      <div class="container" id="tabla">

      </div>
      <div class="row">
        <div class="col-md-offset-5">
          <h1>Insertar Datos</h1>
        </div>
      </div>
      <form>
        <input type="hidden" name="idDato" value="" id="idDato">
        <div class="form-group">
          <select class="form-control" name="idTipoDocumento" id="idTipoDocumento">
            <option selected disabled>Seleccione el Tipo de Documento</option>
            @foreach($tipodocumentos as $tipodocumento)
              <option value="{{$tipodocumento->id}}">{{$tipodocumento->nomTipoDocumento}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Identificación</label>
          <input type="text" class="form-control" name="ideDatos" id="ideDatos" placeholder="Identificación" onkeypress ="return SoloNumerosEnteros(event)">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" class="form-control" name="nomDatos" id="nomDatos" placeholder="Nombre" onkeypress="return soloLetras(event)" onkeyup="cadaprimera(this)">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Apellido</label>
          <input type="text" class="form-control" name="apeDatos" id="apeDatos" placeholder="Apellido" onkeypress="return soloLetras(event)" onkeyup="cadaprimera(this)">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Telefono</label>
          <input type="text" class="form-control" name="telDatos" id="telDatos" placeholder="Telefono" onkeypress ="return SoloNumerosEnteros(event)">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Fecha de Nacimiento</label>
          <input type="date" class="form-control" name="fecDatos" id="fecDatos" placeholder="Fecha de Nacimiento">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Hora</label>
          <input type="text" name="horDatos" class="form-control" id="horDatos" placeholder="Hora">
        </div>
        <button id="guardar" type="button" class="btn btn-success"><i class="icon-plus-sign icon-large" aria-hidden="true"></i> Guardar</button>
      </form>
    </div>
    <script src="{{ asset('js/Ajax.js') }}"></script>
  @endsection
