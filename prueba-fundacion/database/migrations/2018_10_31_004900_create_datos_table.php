<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos', function (Blueprint $table) {
            $table->increments('id');
            $table->String('nomDatos');
            $table->String('apeDatos');
            $table->bigInteger('telDatos');
            $table->bigInteger('ideDatos');
            $table->timestamp('fecDatos');
            $table->Integer('idTipoDocumento')->unsigned();
            $table->foreign('idTipoDocumento')->references('id')->on('tipodocumentos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos');
    }
}
