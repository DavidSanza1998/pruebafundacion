<?php

use Illuminate\Database\Seeder;

use App\Models\Tipodocumento as Tipodocumento;

class TipoDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Tipodocumento::create([
        'nomTipoDocumento' => 'Cedula',
      ]);
      Tipodocumento::create([
        'nomTipoDocumento' => 'Tarjeta Identidad',
      ]);
      Tipodocumento::create([
        'nomTipoDocumento' => 'Pasaporte',
      ]);
      Tipodocumento::create([
        'nomTipoDocumento' => 'Cedula Extranjera',
      ]);
    }
}
