<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipodocumento extends Model
{
  protected $table = "tipodocumentos";
  protected $fillable = ['nomTipoDocumento'];
  protected $guarded = ['id'];
}
