<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dato extends Model
{
  protected $table = "datos";
  protected $fillable = ['nomDatos','apeDatos','telDatos','ideDatos','fecDatos','hijDatos','idTipoDocumento'];
  protected $guarded = ['id'];
}
