<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Dato as Dato;

use App\Models\Tipodocumento as Tipodocumento;

class DatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datos = Dato::select('tipodocumentos.nomTipoDocumento','datos.*')
                    ->join('tipodocumentos', 'tipodocumentos.id','=','datos.idTipoDocumento')
                    ->get();

      return view("Datos/list", compact("datos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipodocumentos = Tipodocumento::select('tipodocumentos.*')->get();

        return view("Datos/create", compact("tipodocumentos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function ajax_save_datos(Request $request)
    {
      //dd($request->fecDatos." ".$request->horDatos);
      if ($request->idDato!="" || $request->idDato!=null) {
        $datos = Dato::Find($request->idDato);
      }else{
        $datos = new Dato;
      }
      $datos->nomDatos = $request->nomDatos;
      $datos->apeDatos = $request->apeDatos;
      $datos->telDatos = $request->telDatos;
      $datos->ideDatos = $request->ideDatos;
      $datos->fecDatos = $request->fecDatos." ".$request->horDatos;
      $datos->idTipoDocumento = $request->idTipoDocumento;
      $datos->save();

      return response()->json($datos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datos = Dato::find($id);

        $tipodocumentos = Tipodocumento::select('tipodocumentos.*')->get();

        return view("Datos/update", compact('tipodocumentos','datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $datos = Dato::find($id)->delete();

      $notification = array(
        'message' => 'Se elimino exitosamente el Dato',
        'alert-type' => 'success'
      );

      return redirect('datos')->with($notification);
    }
}
